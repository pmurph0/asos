package com.pmurphy.asos.repository.categories

import com.pmurphy.asos.data.model.CategoriesWrapper
import com.pmurphy.asos.data.source.DataSource
import com.pmurphy.asos.data.source.LocalDataSource
import io.reactivex.Observable
import io.reactivex.functions.BiFunction

/**
 * Created by PeterMurphy on 05/06/2017.
 */
class CategoriesRepositoryImpl(val remoteDataSource: DataSource,
                               val localDataSource: LocalDataSource) : CategoriesRepository {

    override fun getCategories(): Observable<Pair<CategoriesWrapper, CategoriesWrapper>> {
        val localZip = zipMensAndWomens(localDataSource)

        val remoteZip = zipMensAndWomens(remoteDataSource)?.doOnNext({ mensAndWomens ->
            localDataSource.storeMenCategories(mensAndWomens.first)
            localDataSource.storeWomenCategories(mensAndWomens.second)
        })

        return Observable.concat(localZip, remoteZip)
                .filter({pair -> pair != null})
    }

    private fun zipMensAndWomens(dataSource: DataSource): Observable<Pair<CategoriesWrapper, CategoriesWrapper>>? {
        return Observable.zip<CategoriesWrapper, CategoriesWrapper,
                Pair<CategoriesWrapper, CategoriesWrapper>>(
                dataSource.getCategoriesMen(),
                dataSource.getCategoriesWomen(),
                BiFunction { mens, womens -> Pair(mens, womens) })
    }

}
