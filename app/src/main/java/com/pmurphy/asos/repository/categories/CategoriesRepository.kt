package com.pmurphy.asos.repository.categories

import com.pmurphy.asos.data.model.CategoriesWrapper
import io.reactivex.Observable

/**
 * Created by PeterMurphy on 05/06/2017.
 */
interface CategoriesRepository {

    fun getCategories(): Observable<Pair<CategoriesWrapper, CategoriesWrapper>>

}