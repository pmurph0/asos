package com.pmurphy.asos.home.navdrawer

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import com.pmurphy.asos.R

/**
 * Created by PeterMurphy on 07/06/2017.
 */
class NavDrawerContentView(context: Context?, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {

    init {
        LayoutInflater.from(context).inflate(R.layout.home_nav_drawer, this, true)
    }

}

