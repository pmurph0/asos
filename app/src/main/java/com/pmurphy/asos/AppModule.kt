package com.pmurphy.asos

import com.pmurphy.asos.data.api.ApiService
import com.pmurphy.asos.data.source.DataSource
import com.pmurphy.asos.data.source.LocalDataSource
import com.pmurphy.asos.data.source.InMemoryCache
import com.pmurphy.asos.data.source.RestApiProvider
import com.pmurphy.asos.repository.categories.CategoriesRepository
import com.pmurphy.asos.repository.categories.CategoriesRepositoryImpl
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by PeterMurphy on 05/06/2017.
 */
@Module
class AppModule {

    val BASE_URL = "https://mobile.asosservices.com"

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
    }

    @Singleton
    @Provides
    fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideLocalDataSource(): LocalDataSource {
        return InMemoryCache()
    }

    @Singleton
    @Provides
    fun provideRemoteDataSource(apiService: ApiService): DataSource {
        return RestApiProvider(apiService)
    }

    @Singleton
    @Provides
    fun provideCategoriesRepo(localDataSource: LocalDataSource, remoteDataSource: DataSource): CategoriesRepository {
        return CategoriesRepositoryImpl(remoteDataSource, localDataSource)
    }

}