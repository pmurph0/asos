package com.pmurphy.asos

import android.app.Application

/**
 * Created by PeterMurphy on 05/06/2017.
 */
class AsosApp : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder().build()
    }

}