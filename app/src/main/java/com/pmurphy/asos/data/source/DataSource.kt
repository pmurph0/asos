package com.pmurphy.asos.data.source

import com.pmurphy.asos.data.model.CategoriesWrapper
import io.reactivex.Observable

/**
 * Created by PeterMurphy on 05/06/2017.
 */
interface DataSource {

    fun getCategoriesMen() : Observable<CategoriesWrapper>
    fun getCategoriesWomen() : Observable<CategoriesWrapper>

}