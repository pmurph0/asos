package com.pmurphy.asos.data.api

import com.pmurphy.asos.data.model.CategoriesWrapper
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * Created by PeterMurphy on 05/06/2017.
 */
interface ApiService {

    @GET("/sampleapifortest/cats_women.json")
    fun getWomenCategories(): Observable<CategoriesWrapper>

    @GET("/sampleapifortest/cats_men.json")
    fun getMenCategories(): Observable<CategoriesWrapper>

}