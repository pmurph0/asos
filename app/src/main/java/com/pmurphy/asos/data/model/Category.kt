package com.pmurphy.asos.data.model

/**
 * Created by PeterMurphy on 04/06/2017.
 */
data class Category(val CategoryId: String, val Name: String, val ProductCount: Int)