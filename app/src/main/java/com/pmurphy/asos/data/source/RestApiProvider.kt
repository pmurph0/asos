package com.pmurphy.asos.data.source

import com.pmurphy.asos.data.api.ApiService
import com.pmurphy.asos.data.model.CategoriesWrapper
import io.reactivex.Observable

/**
 * Created by PeterMurphy on 05/06/2017.
 */
class RestApiProvider(val service: ApiService) : DataSource {

    override fun getCategoriesMen(): Observable<CategoriesWrapper> {
        return service.getMenCategories()
    }

    override fun getCategoriesWomen(): Observable<CategoriesWrapper> {
        return service.getWomenCategories()
    }
}