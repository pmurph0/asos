package com.pmurphy.asos.data.source

import com.pmurphy.asos.data.model.CategoriesWrapper

/**
 * Created by PeterMurphy on 06/06/2017.
 */
interface LocalDataSource: DataSource {

    fun storeMenCategories(mensCategoriesWrapper: CategoriesWrapper)

    fun storeWomenCategories(womensCategoriesWrapper: CategoriesWrapper)

}