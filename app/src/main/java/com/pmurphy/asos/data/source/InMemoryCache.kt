package com.pmurphy.asos.data.source

import com.pmurphy.asos.data.model.CategoriesWrapper
import io.reactivex.Completable
import io.reactivex.Observable

/**
 * Created by PeterMurphy on 06/06/2017.
 */
class InMemoryCache : LocalDataSource {

    private var mensCategories: CategoriesWrapper? = null
    private var womensCategories: CategoriesWrapper? = null

    override fun storeMenCategories(mensCategoriesWrapper: CategoriesWrapper) {
       mensCategories = mensCategoriesWrapper
    }

    override fun getCategoriesMen(): Observable<CategoriesWrapper> =
            if (mensCategories != null) Observable.just(mensCategories)
            else Completable.complete().toObservable()

    override fun storeWomenCategories(womensCategoriesWrapper: CategoriesWrapper) {
        womensCategories = womensCategoriesWrapper
    }

    override fun getCategoriesWomen(): Observable<CategoriesWrapper> =
            if (womensCategories != null) Observable.just(womensCategories)
            else Completable.complete().toObservable()
}