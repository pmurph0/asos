package com.pmurphy.asos.data.model

/**
 * Created by PeterMurphy on 05/06/2017.
 */
data class CategoriesWrapper(val Description: String, val Listing: List<Category>)