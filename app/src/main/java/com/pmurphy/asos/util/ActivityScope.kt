package com.pmurphy.asos.util

import javax.inject.Scope

/**
 * Created by PeterMurphy on 05/06/2017.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope