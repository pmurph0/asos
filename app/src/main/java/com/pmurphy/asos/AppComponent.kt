package com.pmurphy.asos

import com.pmurphy.asos.repository.categories.CategoriesRepository
import dagger.Component
import javax.inject.Singleton

/**
 * Created by PeterMurphy on 05/06/2017.
 */
@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {
    fun getCategoriesRepo(): CategoriesRepository
}