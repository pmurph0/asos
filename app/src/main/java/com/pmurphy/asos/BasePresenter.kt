package com.pmurphy.asos

/**
 * Created by PeterMurphy on 05/06/2017.
 */
interface BasePresenter {

    fun subscribe()

    fun unsubscribe()

}