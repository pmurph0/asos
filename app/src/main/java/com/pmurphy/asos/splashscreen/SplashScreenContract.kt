package com.pmurphy.asos.splashscreen

import com.pmurphy.asos.BasePresenter

/**
 * Created by PeterMurphy on 05/06/2017.
 */
interface SplashScreenContract {

    interface View {
        fun proceedToHome()
    }

    interface Presenter: BasePresenter {
        fun loadCategories()
    }

}