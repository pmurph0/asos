package com.pmurphy.asos.splashscreen

import com.pmurphy.asos.AppComponent
import com.pmurphy.asos.util.ActivityScope
import dagger.Component

/**
 * Created by PeterMurphy on 06/06/2017.
 */
@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(SplashScreenModule::class))
interface SplashScreenComponent {
    fun inject(splashScreenActivity: SplashScreenActivity)
}