package com.pmurphy.asos.splashscreen

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.pmurphy.asos.AsosApp
import com.pmurphy.asos.R
import com.pmurphy.asos.home.HomeActivity
import javax.inject.Inject

class SplashScreenActivity : AppCompatActivity(), SplashScreenContract.View {

    @Inject
    lateinit var presenter: SplashScreenContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        DaggerSplashScreenComponent.builder()
                .appComponent(AsosApp.appComponent)
                .splashScreenModule(SplashScreenModule(this))
                .build()
                .inject(this)

        presenter.subscribe()
    }

    override fun proceedToHome() {
        HomeActivity.launch(this)
    }

}
