package com.pmurphy.asos.splashscreen

import android.util.Log
import com.pmurphy.asos.data.model.CategoriesWrapper
import com.pmurphy.asos.repository.categories.CategoriesRepository
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by PeterMurphy on 06/06/2017.
 */
class SplashScreenPresenter(val view: SplashScreenContract.View,
                            val categoriesRepository: CategoriesRepository)
    : SplashScreenContract.Presenter {

    val compositeDisposable = CompositeDisposable()
    private val  tag = javaClass.simpleName

    override fun subscribe() {
       loadCategories()
    }

    override fun unsubscribe() {
        compositeDisposable.clear()
    }

    override fun loadCategories() {
        val subscription = categoriesRepository.getCategories()
                .subscribe({
                    view.proceedToHome()
                }, { error -> Log.e(tag, "Error getting categories", error) })

        compositeDisposable.add(subscription)
    }
}