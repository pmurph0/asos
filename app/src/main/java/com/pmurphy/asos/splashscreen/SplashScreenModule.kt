package com.pmurphy.asos.splashscreen

import com.pmurphy.asos.repository.categories.CategoriesRepository
import dagger.Module
import dagger.Provides

/**
 * Created by PeterMurphy on 06/06/2017.
 */
@Module
class SplashScreenModule(val view: SplashScreenContract.View) {

    @Provides
    fun provideSplashScreenPresenter(categoriesRepository: CategoriesRepository)
            : SplashScreenContract.Presenter {
        return SplashScreenPresenter(view, categoriesRepository)
    }

}