package com.pmurphy.asos

import com.pmurphy.asos.data.api.ApiService
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.core.IsNull
import org.hamcrest.core.IsNull.notNullValue
import org.junit.Test

import org.junit.Assert.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ApiIntegrationTest {
    @Test
    fun testApi() {
        val retrofit = Retrofit.Builder()
                .baseUrl("https://mobile.asosservices.com")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        val service = retrofit.create(ApiService::class.java)

        val response = service.getMenCategories().blockingFirst()
        assertThat(response, `is`(notNullValue()))
        assertThat(response.Listing, `is`(notNullValue()))
    }
}
